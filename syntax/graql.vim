" Vim syntax file
" Language: graql
" Author: Mario Antonetti
" Version: 0.1

"keywords
syn keyword graqlNativeType thing entity attribute relation role rule

syn keyword graqlValueType long double string boolean datetime

syn keyword graqlCommand define undefine match get insert delete
syn keyword graqlCommand compute

syn keyword graqlQueryModifier offset limit group sort asc desc

syn keyword graqlQueryCompute centrality using

syn keyword graqlStatementProperty abstract as id type isa isa! sub sub! key 
syn keyword graqlStatementProperty has plays relates value regex when then

syn keyword grablOperator or not like

syn keyword graqlLiteralValue true false

syn match graqlIdentifier "\w\(\w*\d*[_\-]*\)\+"

syn region graqlString start=/\v"/ skip=/\v\\./ end=/\v"/
" TODO: add floats, longs, datetimes


" hi link
hi def link graqlNativeType Type
hi def link graqlCommand PreProc
hi def link graqlStatementProperty Label
hi def link graqlIdentifier Identifier
hi def link graqlValueType Structure
hi def link graqlString String
hi def link graqlLiteralValue Boolean
hi def link grablOperator Operator
hi def link graqlQueryModifier Repeat

let b:current_syntax = "sql"
