# vim-graql

I made this because I needed it. Simple syntax highlighting for .graql 
documents. Certainly doesn't cover all cases nor is it perfect, nor does it do
any indenting. I'll be revisiting this, but for now, give it a shot.

```
Plug install 'https://gitlab.com/DiligentDilettante/vim-graql.git'
```

That should be all you need.
